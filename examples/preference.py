import numpy as np
from tqdm import tqdm as tqdm
from joblib import Parallel, delayed
from scipy.stats import truncnorm
from scipy.stats import binom as binom
import os, random
import sys
import gc
import psutil
#import objgraph

# PyTorch stuff
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import torch.autograd as autograd
from torch.autograd import Variable

import sys, os
sys.path.append(os.path.dirname(__file__) + "../source/")
import minebed
import minebed.networks as mn
from torch.optim.lr_scheduler import StepLR
import minebed.static.bed as bed


import matplotlib.pyplot as plt

# ------ FUNCTIONS AND CLASSES ------ #

def simulator_load(index, prior, device):
    length = prior.shape[0]
    ys = np.load('../obs_preference.npy')[:length,index]
    ys_grad = ys
    return ys, ys_grad


def seed_torch(seed=2022):
    random.seed(seed)
    os.environ['PYTHONHASHSEED'] = str(seed)
    np.random.seed(seed)
    torch.manual_seed(seed)
    torch.cuda.manual_seed(seed)
    torch.cuda.manual_seed_all(seed) # if you are using multi-GPU.
    torch.backends.cudnn.benchmark = False
    torch.backends.cudnn.deterministic = True

# ----- SPECIFY CUDA DEVICE ---- #
# device = 'cpu'
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
progress_bar_refresh_rate=0

if __name__ == "__main__":
    
    # ------ FILE INPUT ---------- #
    
    inputs = sys.argv[1:]
    DIM = 1
    LAMBDA = 1e-3
    H = 10

    # Optimisation Params: Psi
    LR_PSI = 1e-3
    STEP_PSI = 2000
    GAMMA_PSI = 1.0  # this would require fine-tuning

    # Optimisation Params: Designs
    LR_D = 1e-2
    STEP_D = 1000
    GAMMA_D = 1.0  # this would require fine-tuning

    # ------ SET HYPERPARAMS ----- #

    SEED = 123459
    DATASIZE = 30000
    N_EPOCHS = 30000 #500 #1000
    # N_STEPS = 50000
    BATCHSIZE = DATASIZE #data_size #1000
    RESET=True
    # FILENAME='trainalt_pharma_dim{}_final30k_lambda{}_H{}'.format(DIM, LAMBDA_PERC, H)

    # set seed (don't know if that works...)
    seed_torch(SEED)

    # ------ RUN CODE ------- #

    # Define Design domains and initialization
    domain_lowbound = 0
    domain_highbound = 24
    domain = np.array([domain_lowbound, domain_highbound])

    # Choose Design to initialize at:
    initial_d = np.random.uniform(domain[0], domain[-1], size=DIM).reshape(-1, 1)

    # ----- Sample parameters and generate data ----- #

    prior = np.load('../prior_preference.npy')[:DATASIZE]
    check = np.load('../obs_preference.npy')

    d = torch.tensor(initial_d, dtype = torch.float, device = device, requires_grad = False)
    X = torch.tensor(prior, dtype = torch.float, device = device, requires_grad = False)

    #dim1, dim2 = prior.reshape(-1, 1).shape[-1], zs[0].reshape(-1,1).shape[-1]+1
    dim1, dim2 = 1, 1

    # define model
    # model = minebed.Net(var1_dim=dim1, var2_dim=dim2, L=1, H=H)
    model = mn.FullyConnected(var1_dim=dim1, var2_dim=dim2, L=1, H=H)
    model.to(device)

    optimizer_psi = optim.Adam(model.parameters(), lr=LR_PSI, amsgrad=True)
    optimizer_design = optim.Adam([d], lr=LR_D, amsgrad=True)

    scheduler_psi = StepLR(optimizer_psi, step_size=STEP_PSI, gamma=GAMMA_PSI)
    scheduler_design = StepLR(optimizer_design, step_size=STEP_D, gamma=GAMMA_D)


    # define scheduler
    lr_lmbda = LAMBDA
    lrd_lmbda = LAMBDA

    bed_sgd = bed.GradientBasedBED(
    model=model, optimizer=optimizer_psi, optimizer_design=optimizer_design,
    scheduler=scheduler_psi, scheduler_design=scheduler_design,
    simulator=simulator_load, prior=X, n_epoch=N_EPOCHS,
    batch_size=BATCHSIZE, design_bounds=domain, device=device, LB_type='NWJ')

    d_list = np.arange(0,20,1,dtype=int)
    # d_list = np.arange(1,24,1,dtype=int)
    # import pdb
    # pdb.set_trace()
    # d_list = np.array([0.551])
    y_size = 10
    print('Start Training:')

    eig_list = []
    reig_list_01 = []
    reig_list_001 = []
    reig_list_0001 = []
    joint = []
    marg = []
    pr = []
    y = []
    checking = []
    for i in d_list:
        print(i)
        # d = torch.tensor(i.reshape(-1,1), dtype=torch.float, device=device, requires_grad=False)
        # # import pdb
        # # pdb.set_trace()
        # d.to(device)
        eig_list.append(bed_sgd.train_psi_2(i))
        # import pdb    
        # pdb.set_trace()
        # asdf = bed_sgd.get_sample(d)
        # pr.append(bed_sgd.prior.cpu().detach().unsqueeze(-1))
        # y.append(asdf[0].cpu().detach())
        # joint.append(asdf[1].cpu().detach())
        # marg.append(asdf[2].cpu().detach())
        # eig_list.append(bed_sgd.mutual[-1])
        # # pdb.set_trace()
        # reig_list_01.append(bed_sgd.evaluate(d, y_size,eps=0.1))
        # reig_list_001.append(bed_sgd.evaluate(d, y_size,eps=0.01))
        # reig_list_0001.append(bed_sgd.evaluate(d, y_size,eps=0.001))
        # checking.append(bed_sgd.designs[:,0,0])
    # import pdb
    # pdb.set_trace()
    # joint= torch.cat(joint,-1).numpy()
    # marg = torch.cat(marg,-1).numpy()
    # pr = torch.cat(pr,-1).numpy()
    # y = torch.cat(y,-1).numpy()
    # import pdb
    # pdb.set_trace()
    np.save('eig_list_pref_30000', eig_list)
    # np.save('d_list.npy', d_list)
    # np.save('joint', joint)
    # np.save('marg', marg)
    # np.save('pr', pr)
    # np.save('y', y)
    #import pdb
    #pdb.set_trace()
    # np.save('reig_list_01', reig_list_01)
    # np.save('reig_list_001',reig_list_001)
    # np.save('reig_list_0001', reig_list_0001)
    #np.save('checking', checking)
    # import pdb
    # pdb.set_trace()
    # plt.plot(d_list, eig_list, label = 'EIG')
    # plt.xlabel("xi")
    # plt.ylabel("EIG")
    # plt.ylim([0,1])
    # plt.show()
    # import pdb
    # pdb.set_trace()
    # print('Start Training:')
    # # bed_sgd.train_psi(d)
    # print('Optimal Design:', bed_sgd.designs[-1])
    # schedule_step = 5000 #int(N_STEPS / 10)
    
    #plt.plot(d_list, eig_list, label = 'EIG')
    #plt.plot(d_list, reig_list_01, label = 'REIG_0.1')
    #plt.plot(d_list, reig_list_001, label = 'REIG_0.01')
    #plt.plot(d_list, reig_list_0001, label = 'REIG_0.001')
    #plt.xlabel('xi 0 to 24')
    #plt.ylabel('EIG vs REGI')
    #plt.legend(loc='best')
    #plt.savefig('eigvreig.png')

    # plt.show()
    # plt.legend(loc='best')
    # # run optimisation
    # loss, mtrained, dtrained, gtrained = minebed.train_alt_minef(
    #     model, prior, initial_d, domain, simulator_pharma, 
    #     n_epoch=N_EPOCHS, n_steps=N_STEPS, lr=1e-3, lr_d=1e-2,
    #     mom_d=0.90, batch_size=BATCHSIZE, reset=RESET, renorm=True, 
    #     lr_lmbda=lr_lmbda, schedule_step=schedule_step, lrd_lmbda=lrd_lmbda)

    # # save data
    # np.savez('./data/{}.npz'.format(FILENAME), loss=loss, designs=dtrained, gradients=gtrained, model=mtrained)
    
