import numpy as np
from tqdm import tqdm as tqdm
from joblib import Parallel, delayed
from scipy.stats import truncnorm
from scipy.stats import binom as binom
import os, random
import sys
import gc
import psutil
#import objgraph

# PyTorch stuff
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import torch.autograd as autograd
from torch.autograd import Variable

import sys, os
sys.path.append(os.path.dirname(__file__) + "../source/")
import minebed
import minebed.networks as mn
from torch.optim.lr_scheduler import StepLR
import minebed.static.bed as bed


import matplotlib.pyplot as plt

# ------ FUNCTIONS AND CLASSES ------ #

def simulator_pharma(d, prior, device):
    # import pdb
    # pdb.set_trace()
    # constants
    D = 400.
    sig_prop = np.sqrt(0.01)
    sig_add = np.sqrt(0.1)
    
    # reshape priors
    ka, ke, V = prior[:,0].reshape(-1, 1), prior[:,1].reshape(-1, 1), prior[:,2].reshape(-1, 1)
    
    # sample data
    ys = list()
    ys_grad = list()

    # import pdb
    # pdb.set_trace()
    for di in d:
        
        # sample noise data
        # eps_1 = np.random.normal(0, sig_prop, prior.shape[0]).reshape(-1, 1)
        # eps_2 = np.random.normal(0, sig_add, prior.shape[0]).reshape(-1, 1)
        eps_1 = torch.empty(prior.shape[0], device = device, dtype = torch.float).normal_(0, sig_prop).reshape(-1,1)
        eps_2 = torch.empty(prior.shape[0], device = device, dtype = torch.float).normal_(0, sig_add).reshape(-1,1)
  
    
        # sample data
        ysi = (D / V) * (ka / (ka - ke)) * (torch.exp(-ke*di) - torch.exp(-ka*di)) * (1 + eps_1) + eps_2
        ys.append(ysi)
    
        # sample gradients
        ysi_grad = (D / V) * (ka / (ka - ke)) * (ka*torch.exp(-ka*di) - ke*torch.exp(-ke*di)) * (1 + eps_1)
        ys_grad.append(ysi_grad)
    
    # import pdb
    # pdb.set_trace()
    # ys = np.array(ys).T[0]
    ys = torch.cat(ys, 1)
    ys_grad = torch.cat(ys_grad,1)
    # ys_grad = np.array(ys_grad).T[0]
    
    return ys, ys_grad


def seed_torch(seed=2022):
    random.seed(seed)
    os.environ['PYTHONHASHSEED'] = str(seed)
    np.random.seed(seed)
    torch.manual_seed(seed)
    torch.cuda.manual_seed(seed)
    torch.cuda.manual_seed_all(seed) # if you are using multi-GPU.
    torch.backends.cudnn.benchmark = False
    torch.backends.cudnn.deterministic = True

# ----- SPECIFY CUDA DEVICE ---- #
# device = 'cpu'
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    
if __name__ == "__main__":
    
    # ------ FILE INPUT ---------- #
    
    inputs = sys.argv[1:]
    DIM = 1
    LAMBDA = 1e-3
    H = 10

    # Optimisation Params: Psi
    LR_PSI = 1e-3
    STEP_PSI = 2000
    GAMMA_PSI = 1.0  # this would require fine-tuning

    # Optimisation Params: Designs
    LR_D = 1e-2
    LR_D_init = LR_D
    STEP_D = 1000
    GAMMA_D = 1.0  # this would require fine-tuning

    # DIM = int(inputs[0])
    # LAMBDA = float(inputs[1])
    # LAMBDA_PERC = int(LAMBDA * 100)
    # H = int(inputs[2])

    # ------ SET HYPERPARAMS ----- #

    y_size = 10
    SEED = 123459
    #DIM = 10
    DATASIZE = 20000
    N_EPOCHS = 30000 #500 #1000
    # N_STEPS = 50000
    BATCHSIZE = DATASIZE #data_size #1000
    RESET=False
    # FILENAME='trainalt_pharma_dim{}_final30k_lambda{}_H{}'.format(DIM, LAMBDA_PERC, H)

    # set seed (don't know if that works...)
    seed_torch(SEED)

    # ------ RUN CODE ------- #

    # Define Design domains and initialization
    domain_lowbound = 0
    domain_highbound = 24
    domain = np.array([domain_lowbound, domain_highbound])

    # Choose Design to initialize at:
    initial_d = np.random.uniform(domain[0], domain[-1], size=DIM).reshape(-1, 1)


    # ----- Sample parameters and generate data ----- #

    # Get regular prior samples
    sig = np.sqrt(0.05)
    # m0, m1, m2 = np.log(1), np.log(0.1), np.log(20)
    m0, m1, m2 = 0.1, np.log(0.1), np.log(20)
    param_0 = np.random.normal(m0, sig, int(DATASIZE/y_size)).reshape(-1,1) # ka
    param_1 = list()
    fill = 0

    while fill < DATASIZE/y_size:

        logka = param_0[fill]
        logke = np.random.normal(m1, sig)

        if logka > logke:
            param_1.append(logke)
            fill += 1

    param_1 = np.array(param_1).reshape(-1, 1)
    param_1 = np.random.normal(m1, sig, int(DATASIZE/y_size)).reshape(-1,1) # ke
    param_2 = np.random.normal(m2, sig, int(DATASIZE/y_size)).reshape(-1,1) # V
    prior = np.hstack((param_0, param_1, param_2)) # compute log(theta)
    prior = np.exp(prior) # compute theta
    prior = prior.repeat(y_size,0)
    # import pdb
    # pdb.set_trace()
    d = torch.tensor(initial_d, dtype = torch.float, device = device, requires_grad = False)
    X = torch.tensor(prior, dtype = torch.float, device = device, requires_grad = False)
    # d.to(device)
    # X.to(device)

    #dim1, dim2 = prior.reshape(-1, 1).shape[-1], zs[0].reshape(-1,1).shape[-1]+1
    dim1, dim2 = prior.shape[-1], DIM

    # define model
    # model = minebed.Net(var1_dim=dim1, var2_dim=dim2, L=1, H=H)
    model = mn.FullyConnected(var1_dim=dim1, var2_dim=dim2, L=1, H=H)
    model.to(device)

    optimizer_psi = optim.Adam(model.parameters(), lr=LR_PSI, amsgrad=True)
    optimizer_design = optim.Adam([d], lr=LR_D, amsgrad=True)

    scheduler_psi = StepLR(optimizer_psi, step_size=STEP_PSI, gamma=GAMMA_PSI)
    scheduler_design = StepLR(optimizer_design, step_size=STEP_D, gamma=GAMMA_D)


    # define scheduler
    lr_lmbda = LAMBDA
    lrd_lmbda = LAMBDA


    bed_sgd = bed.GradientBasedBED(
    model=model, optimizer=optimizer_psi, optimizer_design=optimizer_design,
    scheduler=scheduler_psi, scheduler_design=scheduler_design,
    simulator=simulator_pharma, prior=X, n_epoch=N_EPOCHS,
    batch_size=BATCHSIZE, design_bounds=domain, device=device, LB_type='NWJ')

    d_list = np.arange(1,24,1,dtype=int)
    # import pdb
    # pdb.set_trace()
    # d_list = np.array([0.551])
    print('Start Training:')
    # import pdb
    # pdb.set_trace()
    eig_list = []
    reig_list_01 = []
    reig_list_001 = []
    reig_list_0001 = []
    for i in d_list:
        d = torch.tensor(i.reshape(-1,1), dtype=torch.float, device=device, requires_grad=False)
        # import pdb
        # pdb.set_trace()
        d.to(device)
        bed_sgd.train_psi(d)
        eig_list.append(bed_sgd.mutual[-1])
        # import pdb
        # pdb.set_trace()
        reig_list_01.append(bed_sgd.evaluate_org(d, y_size,eps=0.1))
        reig_list_001.append(bed_sgd.evaluate_org(d, y_size,eps=0.01))
        reig_list_0001.append(bed_sgd.evaluate_org(d, y_size,eps=0.001))

    np.save('eig_list_pk.npy', eig_list)
    np.save('d_list_pk.npy', d_list)
    np.save('reig_list_01_pk', reig_list_01)
    np.save('reig_list_001_pk',reig_list_001)
    np.save('reig_list_0001_pk', reig_list_0001)

    # import pdb
    # pdb.set_trace()
    # plt.plot(d_list, eig_list, label = 'EIG')
    # plt.xlabel("xi")
    # plt.ylabel("EIG")
    # plt.ylim([0,1])
    # plt.show()
    # import pdb
    # pdb.set_trace()
    # print('Start Training:')
    # # bed_sgd.train_psi(d)
    # print('Optimal Design:', bed_sgd.designs[-1])
    # import pdb
    # pdb.set_trace()
    # schedule_step = 5000 #int(N_STEPS / 10)
    
    plt.plot(d_list, eig_list, label = 'EIG')
    # lt.plot(d_list, eig_list, label = 'EIG')
    plt.plot(d_list, reig_list_01, label = 'REIG_0.1')
    plt.plot(d_list, reig_list_001, label = 'REIG_0.01')
    plt.plot(d_list, reig_list_0001, label = 'REIG_0.001')
    plt.xlabel('xi 0 to 24')
    plt.ylabel('EIG vs REGI')
    plt.legend(loc='best')
    plt.savefig('eigvreig.png')

    # plt.show()
    # plt.legend(loc='best')
    # # run optimisation
    # loss, mtrained, dtrained, gtrained = minebed.train_alt_minef(
    #     model, prior, initial_d, domain, simulator_pharma, 
    #     n_epoch=N_EPOCHS, n_steps=N_STEPS, lr=1e-3, lr_d=1e-2,
    #     mom_d=0.90, batch_size=BATCHSIZE, reset=RESET, renorm=True, 
    #     lr_lmbda=lr_lmbda, schedule_step=schedule_step, lrd_lmbda=lrd_lmbda)

    # # save data
    # np.savez('./data/{}.npz'.format(FILENAME), loss=loss, designs=dtrained, gradients=gtrained, model=mtrained)
    
