import numpy as np

# PyTorch stuff
import torch
from scipy.optimize import minimize


class REIG(torch.autograd.Function):
    """
    We can implement our own custom autograd Functions by subclassing
    torch.autograd.Function and implementing the forward and backward passes
    which operate on Tensors.
    """

    @staticmethod
    def forward(ctx, input, epsilon=1e-3, max_bounds = 1e10):
        """
        In the forward pass we receive a Tensor containing the input and return
        a Tensor containing the output. ctx is a context object that can be used
        to stash information for backward computation. You can cache arbitrary
        objects for use in the backward pass using the ctx.save_for_backward method.
        """
        # import pdb
        # pdb.set_trace()
        #
        intmed = lambda_cal_max_scipy(input, epsilon)
        ctx.save_for_backward(input, torch.Tensor(np.array(epsilon)), torch.Tensor(np.array(intmed.x)))
        import pdb
        pdb.set_trace()
        return torch.Tensor(np.array(intmed.fun))

    @staticmethod
    def backward(ctx, grad_output):
        """
        In the backward pass we receive a Tensor containing the gradient of the loss
        with respect to the output, and we need to compute the gradient of the loss
        with respect to the input.
        """

        input, epsilon, lambd = ctx.saved_tensors
        import pdb
        pdb.set_trace()
        return grad_output*torch.softmax(input/lambd,0), None #* torch.softmax(d/lambd)


# ------ MINE-F LOSS FUNCTION ------ #

def minef_loss(x_sample, y_sample, model, device):

    # Shuffle y-data for the second expectation
    idxs = np.random.choice(
        range(len(y_sample)), size=len(y_sample), replace=False)
    # We need y_shuffle attached to the design d
    y_shuffle = y_sample[idxs]

    # Get predictions from network
    # import pdb
    # pdb.set_trace()
    pred_joint = model(x_sample, y_sample)
    pred_marg = model(x_sample, y_shuffle)

    # Compute the MINE-f (or NWJ) lower bound
    Z = torch.tensor(np.exp(1), device=device, dtype=torch.float)
    mi_ma = torch.mean(pred_joint) - torch.mean(
        torch.exp(pred_marg) / Z + torch.log(Z) - 1)

    # we want to maximize the lower bound; PyTorch minimizes
    loss = - mi_ma

    return loss

def minef_loss_sample(x_sample, y_sample, model, device):

    # Shuffle y-data for the second expectation
    idxs = np.random.choice(
        range(len(y_sample)), size=len(y_sample), replace=False)
    # We need y_shuffle attached to the design d
    y_shuffle = y_sample[idxs]

    # Get predictions from network
    # import pdb
    # pdb.set_trace()
    pred_joint = model(x_sample, y_sample)
    pred_marg = model(x_sample, y_shuffle)

    # Compute the MINE-f (or NWJ) lower bound
    Z = torch.tensor(np.exp(1), device=device, dtype=torch.float)
    mi_ma = torch.mean(pred_joint) - torch.mean(
        torch.exp(pred_marg) / Z + torch.log(Z) - 1)

    # we want to maximize the lower bound; PyTorch minimizes
    loss = - mi_ma

    return y_sample, pred_joint, pred_marg

def rminef_loss(x_sample, y_sample, model, device, y_size, eps=0.1):

    # Shuffle y-data for the second expectation
    idxs = np.random.choice(
        range(len(y_sample)), size=len(y_sample), replace=False)
    # We need y_shuffle attached to the design d
    y_shuffle = y_sample[idxs]

    # Get predictions from network
    pred_joint = model(x_sample, y_sample)
    pred_marg = model(x_sample, y_shuffle)
    
    pred_marg_ = pred_marg.reshape(int(y_size),int(len(x_sample)/y_size),1)
    pred_marg_ey = torch.mean(torch.exp(pred_marg_-1),0)
    pred_joint_ = pred_joint.reshape(int(y_size),int(len(x_sample)/y_size),1)
    pred_joint_ey = torch.mean(pred_joint_,0)

    # Compute the MINE-f (or NWJ) lower bound
    Z = torch.tensor(np.exp(1), device=device, dtype=torch.float)

    terms = pred_joint_ey - pred_marg_ey
    reig = REIG.apply
    opt_result = reig(terms, eps)
    import pdb
    pdb.set_trace()
    mi_ma = torch.mean(pred_joint) - torch.mean(
        torch.exp(pred_marg) / Z + torch.log(Z) - 1)

    return opt_result, -mi_ma
   

def lambda_cal_scipy(intmed, epsilon, max_bounds = 1e10):
    intmed = -intmed.numpy()
    max_intmed = max(intmed)
    intmed= intmed - max_intmed
    def lambda_scipy_curry(lam):
        return lam* epsilon + lam*np.log(np.sum(np.exp(intmed/lam))/len(intmed)) + max_intmed
    lam = np.array(1.4)
    bnds = ((1e-32,max_bounds),)
    result = minimize(lambda_scipy_curry, lam, method='Nelder-Mead', bounds=bnds)
    return result 

def lambda_cal_max_scipy(intmed, epsilon, max_bounds = 1e10):
    import pdb
    pdb.set_trace()
    intmed = intmed.numpy()
    max_intmed = max(intmed)
    intmed= intmed - max_intmed
    def lambda_scipy_curry(lam):
        return lam* epsilon + lam*np.log(np.sum(np.exp(intmed/lam))/len(intmed)) + max_intmed
    lam = np.array(1.4)
    bnds = ((1e-32,max_bounds),)
    result = minimize(lambda_scipy_curry, lam, method='Nelder-Mead', bounds=bnds)
    return result

def minef_gradients(x_sample, y_sample, ygrads, model, device):

    # obtain marginal data and log-likelihood gradients
    idx = np.random.permutation(len(y_sample))
    y_shuffle = y_sample[idx]
    ygrads_shuffle = ygrads[idx]

    # Need to create new tensors for the autograd computation to work;
    # This is because y is not a leaf variable in the computation graph
    x_sample = torch.tensor(
        x_sample, dtype=torch.float, device=device, requires_grad=True)
    y_sample = torch.tensor(
        y_sample, dtype=torch.float, device=device, requires_grad=True)
    y_shuffle = torch.tensor(
        y_shuffle, dtype=torch.float, device=device, requires_grad=True)

    # Get predictions from network
    pred_joint = model(x_sample, y_sample)
    pred_marg = model(x_sample, y_shuffle)

    # Compute gradients of lower bound with respect to data y
    dIdy_joint = torch.autograd.grad(
        pred_joint.sum(), (x_sample, y_sample), retain_graph=True)[1].data
    dIdy_marg = torch.autograd.grad(
        pred_marg.sum(), (x_sample, y_shuffle), retain_graph=True)[1].data

    # Compute gradient through forward differentiation
    dE1 = torch.mean(dIdy_joint * ygrads, axis=0)
    Z = torch.tensor(np.exp(1), device=device, dtype=torch.float)
    dE2 = torch.mean(
        dIdy_marg * ygrads_shuffle * torch.exp(pred_marg) / Z, axis=0)

    dI = dE1.reshape(-1, 1) - dE2.reshape(-1, 1)

    # we want to maximize the lower bound; PyTorch minimizes
    loss_gradients = - dI

    return loss_gradients
